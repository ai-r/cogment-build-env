# cogment-build-env

[![Retrieve from Docker Hub](https://img.shields.io/docker/v/cogment/orchestrator-build-env?sort=semver&style=flat-square)](https://hub.docker.com/repository/docker/cogment/orchestrator-build-env) [![Apache 2 License](https://img.shields.io/badge/license-Apache%202-green)](./LICENSE) [![Changelog](https://img.shields.io/badge/-Changelog%20-blueviolet)](./CHANGELOG.md)

Standardized build environment for cogment.

This Docker image is primarily meant to be used as a base for multi-stage builds.
