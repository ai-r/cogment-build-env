FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=America/New_York

# Necessary
RUN apt-get update && apt-get install -y \
  build-essential \
  cmake \
  git \
  clang \
  autogen \
  autoconf \
  libtool \
  libgflags-dev \
  zlib1g-dev \
  npm \
  wget \
  libcurl4-openssl-dev \
  libssl-dev \
  uuid-dev \
  libpulse-dev

# Useful
RUN apt-get install -y \
  clang-format \
  clang-tidy \
  valgrind \
  vim \
  gdb \
  jq && \
  rm -rf /var/lib/apt/lists/*


ARG BUILD_TYPE=Release

ARG GRPC_RELEASE_TAG=v1.41.1
ARG GRPC_WEB_RELEASE_TAG=1.2.0
ARG PROMETHEUS_CPP_RELEASE_TAG=v0.6.0
ARG SPDLOG_RELEASE_TAG=v1.x
ARG YAML_CPP_RELEASE_TAG=yaml-cpp-0.6.2
ARG GOOGLETEST_RELEASE_TAG=v1.10.x

# GoogleTest
RUN git clone -b ${GOOGLETEST_RELEASE_TAG} --single-branch https://github.com/google/googletest.git /var/local/src/gtest && \
    cd /var/local/src/gtest && \
    mkdir _bld && \
    cd _bld && \
    cmake -DCMAKE_BUILD_TYPE=${BUILD_TYPE} .. && \
    make -j$(nproc) install && \
    cd / && \
    rm -rf /var/local/src/gtest

# gRPC
RUN git clone -b ${GRPC_RELEASE_TAG} --single-branch https://github.com/grpc/grpc /var/local/src/grpc && \
    cd /var/local/src/grpc && \
    git submodule update --init --recursive
RUN cd /var/local/src/grpc && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=${BUILD_TYPE} -DgRPC_INSTALL=ON -DgRPC_BUILD_TESTS=OFF .. && \
    make CXXFLAGS="-g" CFLAGS="-g" -j$(nproc) install
RUN cd /var/local/src/grpc/third_party/abseil-cpp && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=${BUILD_TYPE} -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE .. && \
    make CXXFLAGS="-g" CFLAGS="-g" -j$(nproc) install
RUN cd / && \
    rm -rf /var/local/src/grpc

# Setting up the C++ and Python gRPC plugins for gRPC
RUN ln -s /usr/local/bin/grpc_cpp_plugin /usr/local/bin/protoc-gen-grpc_cpp && \
    ln -s /usr/local/bin/grpc_python_plugin /usr/local/bin/protoc-gen-grpc_python

# grpc web
ARG GRPC_WEB_SOURCE=https://github.com/grpc/grpc-web/releases/download/${GRPC_WEB_RELEASE_TAG}/protoc-gen-grpc-web-${GRPC_WEB_RELEASE_TAG}-linux-x86_64
RUN wget -O /usr/local/bin/protoc-gen-grpc_web ${GRPC_WEB_SOURCE} && \
    chmod +x /usr/local/bin/protoc-gen-grpc_web

# protoc
RUN npm -g install ts-protoc-gen

# yaml-cpp
RUN git clone -b ${YAML_CPP_RELEASE_TAG} --single-branch  https://github.com/jbeder/yaml-cpp.git /var/local/src/yaml-cpp && \
    cd /var/local/src/yaml-cpp && \
    git submodule update --init --recursive && \
    mkdir _bld && cd _bld && \
    cmake -DCMAKE_BUILD_TYPE=${BUILD_TYPE} .. && make -j$(nproc) && make install && \
    rm -rf /var/local/src/yaml-cpp

# spdlog
RUN git clone -b ${SPDLOG_RELEASE_TAG} --single-branch  https://github.com/gabime/spdlog.git /var/local/src/spdlog && \
    cd /var/local/src/spdlog && \
    git submodule update --init --recursive && \
    mkdir _bld && cd _bld && \
    cmake -DCMAKE_BUILD_TYPE=${BUILD_TYPE} .. -DSPDLOG_BUILD_TESTS=OFF && make -j$(nproc) && make install && \
    rm -rf /var/local/src/spdlog

# prometheus cpp client
RUN git clone -b ${PROMETHEUS_CPP_RELEASE_TAG} --single-branch --depth=1 https://github.com/jupp0r/prometheus-cpp.git /var/local/src/prometheus-cpp && \
    cd /var/local/src/prometheus-cpp && \
    git submodule update --init --recursive && \
    mkdir _bld && cd _bld && \
    cmake -DCMAKE_BUILD_TYPE=${BUILD_TYPE} .. && make -j$(nproc) && make install && \
    rm -rf /var/local/src/prometheus-cpp

WORKDIR /workspace
